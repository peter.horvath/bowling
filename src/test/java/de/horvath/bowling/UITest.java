package de.horvath.bowling;

import junit.framework.TestCase;

import de.horvath.bowling.UI;
import de.horvath.bowling.logic.Game;
import de.horvath.bowling.logic.LogicException;

public class UITest extends TestCase {
    /**
     * Simple testcase for a simple UI instance lifecycle scenario. If it doesn't stop
     * with an exception, it is okay.
     */
    public void testUI() throws LogicException {
    	UI ui = new UI();
    	Game game = new Game();
    	
    	ui.parseCmdlineInput(new String[]{"[[3, 4], [3, 4], [3, 4], [3, 4], [3, 4], "
    									+ "[3, 4], [3, 4], [3, 4], [3, 4], [3, 4]]"});
    	
    	ui.playGame(game);
    	ui.printResultsToStdout();
    	
        assert true;
    }
}