package de.horvath.bowling;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import de.horvath.bowling.logic.Game;
import de.horvath.bowling.logic.LogicException;

public class IntegrationTest {
	private void integrationTest(String input, String expectedResult) {
		UI ui = new UI();
		Game game = new Game();
		
		boolean expectException = false;
		Integer[] expectedResultByFrame = new Integer[10];
		ArrayList<Integer> calculatedResultByFrame = null;
    	String expectedExceptionCode = null;
    	LogicException gotException = null;

		try {
			String[] expectedResultStrings = StringUtils.split(expectedResult, ",");
	    	for (Integer idx = 0; idx<10; idx++)
	    		expectedResultByFrame[idx] = Integer.parseInt(expectedResultStrings[idx]);
		} catch (NumberFormatException e) {
			expectException = true;
		} catch (IndexOutOfBoundsException e) {
			expectException = true;
		}
		
		if (expectException) {
			expectedExceptionCode = expectedResult;
		}
		
    	try {
			ui.parseCmdlineInput(new String[]{input});
    		ui.playGame(game);
    		calculatedResultByFrame = game.getResult();
    	} catch (LogicException e) {
    		gotException = e;
    	}
		
    	String result="";
    	
    	String debugResult="Testing input "+game.toString()+"\n"+
    			"Expecting ";
    	
		if (expectException) {
			debugResult+="exception "+expectedExceptionCode;
		} else {
			debugResult+="result "+expectedResult;
		}
		
		debugResult+="\nGot ";

		if (gotException != null) {
			debugResult+="exception "+gotException.getCode()+"\n";
			result = gotException.getCode();
		} else {
			debugResult+="result ";
			if (!expectException) {
		    	for (Integer idx=0; idx<10; idx++) {
		    		debugResult+="Frame "+game.getFrames().get(idx).toString() +
		    				" Expected: " + expectedResultByFrame[idx] +
		    				" Calculated: " + calculatedResultByFrame.get(idx) + "\n";
		    	}
			} else {
				debugResult+=game.toString()+"\n";
			}
	    	result = StringUtils.join(calculatedResultByFrame,",");
		}
		
		if (!result.equals(expectedResult))
			System.err.println(debugResult);
		assert result.equals(expectedResult);
	}
	
	public void testExample1() throws LogicException {
		integrationTest("[[1,4],[4,5],[6,4],[5,5],[10],[0,1],[7,3],[6,4],[10],[2,8,6]]",
			"5,14,29,49,60,61,77,97,117,133");
	}
	
	public void testExample2() throws LogicException {
		integrationTest("[[10],[10],[10],[10],[10],[10],[10],[10],[10],[10,10,10]]",
			"30,60,90,120,150,180,210,240,270,300");
	}

	public void testStrikeBonusOverlapTwoFrames() throws LogicException {
		integrationTest("[[1,4],[4,5],[6,4],[5,5],[10],[0,1],[7,3],[10],[10],[2,8,6]]",
			"5,14,29,49,60,61,81,103,123,139");
	}

	public void testTooFewBallsInFrame2() throws LogicException {
		integrationTest("[[1,4],[4],[6,4],[5,5],[10],[0,1],[7,3],[6,4],[10],[2,8,6]]",
			"PREV_FRAME_NOT_READY");
	}

	public void testTooManyBallsInFrame2Normal() throws LogicException {
		integrationTest("[[1,4],[4,5,8],[6,4],[5,5],[10],[0,1],[7,3],[6,4],[10],[2,8,6]]",
			"READY_HIT");
	}
			
	public void testTooManyBallsInFrame5Strike() throws LogicException {
		integrationTest("[[1,4],[4,5],[6,4],[5,5],[6,5],[0,1],[7,3],[6,4],[10],[2,8,6]]",
			"TOO_MANY_KNOCKS");
	}

	public void testTooFewBallsInFrame10Normal() throws LogicException {
		integrationTest("[[1,4],[4,5],[6,4],[5,5],[10],[0,1],[7,3],[6,4],[10],[2]]",
			"NO_RESULT");
	}
	
	public void testTooManyBallsInFrame10Normal() throws LogicException {
		integrationTest("[[1,4],[4,5],[6,4],[5,5],[10],[0,1],[7,3],[6,4],[10],[2,8,6,5]]",
			"READY_HIT");
	}
	
	public void testTooFewBallsInFrame10Spare() throws LogicException {
		integrationTest("[[1,4],[4,5],[6,4],[5,5],[10],[0,1],[7,3],[6,4],[10],[2,8]]",
			"NO_RESULT");
	}
	
	public void testTooManyBallsInFrame10Spare() throws LogicException {
		integrationTest("[[1,4],[4,5],[6,4],[5,5],[10],[0,1],[7,3],[6,4],[10],[2,8,6,8]]",
			"READY_HIT");
	}
	
	public void testTooFewBallsInFrame10Strike() throws LogicException {
		integrationTest("[[1,4],[4,5],[6,4],[5,5],[10],[0,1],[7,3],[6,4],[10],[4,6]]",
			"NO_RESULT");
	}
	
	public void testTooManyBallsInFrame10Strike() throws LogicException {
		integrationTest("[[1,4],[4,5],[6,4],[5,5],[10],[0,1],[7,3],[6,4],[10],[4,6,7,8]]",
			"READY_HIT");
	}
}