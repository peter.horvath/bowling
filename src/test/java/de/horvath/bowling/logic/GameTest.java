package de.horvath.bowling.logic;

import junit.framework.TestCase;

import de.horvath.bowling.UI;
import de.horvath.bowling.logic.Game;

public class GameTest extends TestCase {
	public void testGetLastFrameEmpty() {
		Game game = new Game();
		try {
			game.getLastFrame();
		} catch (LogicException e) {
			if (e.getCode().equals("NO_FRAMES_YET"))
				return;
		}
		assert false;
	}
	
	public void testGetLastFrameNotEmpty() throws LogicException {
		Game game = new Game();
		Frame frame = game.newFrame();
		assert frame == game.getLastFrame();
	}
	
	public void testReady() throws LogicException {
		Game game = new Game();
		Frame frame = game.newFrame();
		frame.newBall(5);
		game.ready();
		try {
			game.ready();
		} catch (LogicException e) {
			if (e.getCode().equals("MULTIPLE_READY"))
				return;
		}
		assert false;
	}
	
	public void testTooManyFrames() throws LogicException {
		Game game = new Game();
		for (Integer idx = 0; idx < 10; idx++) {
			game.newFrame();
			game.newBall(3);
			game.newBall(5);
		}
		try {
			game.newFrame();
		} catch (LogicException e) {
			if (e.getCode().equals("TOO_MANY_FRAMES"))
				return;
		}
		assert false;
	}
	
	public void testInvalidKnocks() throws LogicException {
		Integer knocks[] = new Integer[]{-8, -1, 11, 72};
		for (Integer knock : knocks) {
			Game game = new Game();
			game.newFrame();
			boolean valid = true;
			try {
				game.newBall(knock);
			} catch (LogicException e) {
				if (e.getCode().equals("INVALID_KNOCK_NO"))
					valid = false;
			}
			assert !valid;
		}
		assert true;
	}
	
	public void testValidHits() throws LogicException {
		for (Integer hit = 0; hit < 10; hit++) {
			Game game = new Game();
			Frame frame = game.newFrame();
			frame.newBall(hit);
		}
		assert true;
	}
	
	public void testGetResultsNotReady() throws LogicException {
		UI ui = new UI();
		Game game = new Game();
    	ui.parseCmdlineInput(new String[]{"[[3, 4], [3, 4], [3, 4], [3, 4], [3, 4], "
				+ "[3, 4], [3, 4], [3, 4], [3, 4], [3, 4]]"});
    	try {
    		game.getResult();
    	} catch (LogicException e) {
    		if (e.getCode().equals("NO_RESULT"))
    			return;
    	}
    	assert false;
	}
	
	public void testGetResultsReady() throws LogicException {
		UI ui = new UI();
		Game game = new Game();
    	ui.parseCmdlineInput(new String[]{"[[3, 4], [3, 4], [3, 4], [3, 4], [3, 4], "
				+ "[3, 4], [3, 4], [3, 4], [3, 4], [3, 4]]"});
    	ui.playGame(game);
    	game.getResult();
    	assert true;
	}
}