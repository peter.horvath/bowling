package de.horvath.bowling.logic;

import junit.framework.TestCase;

import de.horvath.bowling.UI;
import de.horvath.bowling.logic.Game;

public class FrameTest extends TestCase {
	public void testStrike() throws LogicException {
		Frame frame = new Frame();
		frame.newBall(10);
		assert frame.getPinsLeft()==0;
		assert !frame.getSpare();
		assert frame.getStrike();
		assert frame.getReady();
	}
	
	public void testNotStrike() throws LogicException {
		Frame frame = new Frame();
		frame.newBall(5);
		frame.newBall(2);
		assert frame.getPinsLeft()==3;
		assert !frame.getSpare();
		assert !frame.getStrike();
		assert frame.getReady();
	}
	
	public void testSpare() throws LogicException {
		Frame frame = new Frame();
		frame.newBall(5);
		frame.newBall(5);
		assert frame.getPinsLeft()==0;
		assert frame.getSpare();
		assert !frame.getStrike();
		assert frame.getReady();
	}
	
	public void testNotSpare() throws LogicException {
		Frame frame = new Frame();
		frame.newBall(10);
		assert frame.getPinsLeft()==0;
		assert !frame.getSpare();
		assert frame.getStrike();
		assert frame.getReady();
	}
	
	public void testStrike10() throws LogicException {
		Frame frame = new Frame();
		frame.setTenth(true);
		frame.newBall(10);
		frame.newBall(2);
		frame.newBall(4);
		assert frame.getPinsLeft()==10;
		assert !frame.getSpare();
		assert frame.getStrike();
		assert frame.getReady();
	}
	
	public void testNotStrike10() throws LogicException {
		Frame frame = new Frame();
		frame.setTenth(true);
		frame.newBall(6);
		frame.newBall(2);
		assert frame.getPinsLeft()==2;
		assert !frame.getSpare();
		assert !frame.getStrike();
		assert frame.getReady();
	}
	
	public void testSpare10() throws LogicException {
		Frame frame = new Frame();
		frame.setTenth(true);
		frame.newBall(4);
		frame.newBall(6);
		frame.newBall(1);
		assert frame.getPinsLeft()==9;
		assert frame.getSpare();
		assert !frame.getStrike();
		assert frame.getReady();
	}
	
	public void testNotSpare10() throws LogicException {
		Frame frame = new Frame();
		frame.setTenth(true);
		frame.newBall(10);
		frame.newBall(4);
		frame.newBall(3);
		assert frame.getPinsLeft()==10;
		assert !frame.getSpare();
		assert frame.getStrike();
		assert frame.getReady();
	}
	
	public void testSumBallsFrom() throws LogicException {
		UI ui = new UI();
		Game game = new Game();
    	ui.parseCmdlineInput(new String[]{
    		"[[1,4],[4,5],[6,4],[5,5],[10],[0,1],[7,3],[6,4],[10],[2,8,6]]"
    	});
    	ui.playGame(game);
    	
    	Frame frame7 = game.getFrames().get(7);
    	
    	Integer result = frame7.sumBallsFrom(1, 4);
    	System.err.println("sumBallsFrom results: "+result);
    	assert result == 24;
	}
	
	public void testReadyNormal() throws LogicException {
		Frame frame = new Frame();
		frame.newBall(2);
		frame.newBall(8);
		assert frame.getPinsLeft()==0;
		assert frame.getSpare();
		assert !frame.getStrike();
		assert frame.getReady();
	}
	
	public void testNotReadyNormal() throws LogicException {
		Frame frame = new Frame();
		frame.newBall(5);
		assert frame.getPinsLeft()==5;
		assert !frame.getSpare();
		assert !frame.getStrike();
		assert !frame.getReady();
	}
	
	public void testReadySpare() throws LogicException {
		Frame frame = new Frame();
		frame.newBall(2);
		frame.newBall(8);
		assert frame.getPinsLeft() == 0;
		assert frame.getSpare();
		assert !frame.getStrike();
		assert frame.getReady();
	}
	
	// there is no "testReadyNotSpare" -- a non-10th frame is always ready, if it is spare
	
	public void testReadyStrike() throws LogicException {
		Frame frame = new Frame();
		frame.newBall(10);
		assert frame.getPinsLeft()==0;
		assert !frame.getSpare();
		assert frame.getStrike();
		assert frame.getReady();
	}

	// there is no "testReadyNotStrike" -- a non-10th frame is always ready, if it is spare

	public void testReadyNormal10() throws LogicException {
		Frame frame = new Frame();
		frame.setTenth(true);
		frame.newBall(2);
		frame.newBall(7);
		assert frame.getPinsLeft()==1;
		assert !frame.getSpare();
		assert !frame.getStrike();
		assert frame.getReady();
	}
	
	public void testNotReadyNormal10() throws LogicException {
		Frame frame = new Frame();
		frame.setTenth(true);
		frame.newBall(5);
		assert frame.getPinsLeft()==5;
		assert !frame.getSpare();
		assert !frame.getStrike();
		assert !frame.getReady();
	}
	
	public void testReadySpare10() throws LogicException {
		Frame frame = new Frame();
		frame.setTenth(true);
		frame.newBall(2);
		frame.newBall(8);
		frame.newBall(5);
		assert frame.getPinsLeft()==5;
		assert frame.getSpare();
		assert !frame.getStrike();
		assert frame.getReady();
	}
	
	public void testNotReadySpare10() throws LogicException {
		Frame frame = new Frame();
		frame.setTenth(true);
		frame.newBall(2);
		frame.newBall(8);
		assert frame.getPinsLeft()==10;
		assert frame.getSpare();
		assert !frame.getStrike();
		assert !frame.getReady();
	}
	
	public void testReadyStrike10() throws LogicException {
		Frame frame = new Frame();
		frame.setTenth(true);
		frame.newBall(10);
		frame.newBall(2);
		frame.newBall(7);
		assert frame.getPinsLeft()==10;
		assert !frame.getSpare();
		assert frame.getStrike();
		assert frame.getReady();
	}

	public void testNotReadyStrike10() throws LogicException {
		Frame frame = new Frame();
		frame.setTenth(true);
		frame.newBall(10);
		frame.newBall(2);
		assert frame.getPinsLeft()==10;
		assert !frame.getSpare();
		assert frame.getStrike();
		assert !frame.getReady();
	}
	
	public void testNewBallNormal() throws LogicException {
		Frame frame = new Frame();
		frame.newBall(3);
		frame.newBall(4);
		
		try {
			frame.newBall(8);
		} catch (LogicException e) {
			if (!e.getCode().equals("READY_HIT"))
				throw e;
		}
		assert true;
	}
	
	public void testNewBallSpare() throws LogicException {
		Frame frame = new Frame();
		frame.newBall(3);
		frame.newBall(7);
		
		try {
			frame.newBall(8);
		} catch (LogicException e) {
			if (!e.getCode().equals("READY_HIT"))
				throw e;
		}
		assert true;
	}
	
	public void testNewBallStrike() throws LogicException {
		Frame frame = new Frame();
		frame.newBall(10);
		
		try {
			frame.newBall(8);
		} catch (LogicException e) {
			if (!e.getCode().equals("READY_HIT"))
				throw e;
		}
		assert true;
	}
	
	public void testNewBallNormal10() throws LogicException {
		Frame frame = new Frame();
		frame.setTenth(true);
		frame.newBall(3);
		frame.newBall(4);
		
		try {
			frame.newBall(8);
		} catch (LogicException e) {
			if (!e.getCode().equals("READY_HIT"))
				throw e;
		}
		assert true;
	}
	
	public void testNewBallSpare10() throws LogicException {
		Frame frame = new Frame();
		frame.setTenth(true);
		frame.newBall(3);
		frame.newBall(7);
		frame.newBall(5);
		
		try {
			frame.newBall(8);
		} catch (LogicException e) {
			if (!e.getCode().equals("READY_HIT"))
				throw e;
		}
		assert true;
	}
	
	public void testNewBallStrike10() throws LogicException {
		Frame frame = new Frame();
		frame.setTenth(true);
		frame.newBall(10);
		frame.newBall(2);
		frame.newBall(7);
		
		try {
			frame.newBall(8);
		} catch (LogicException e) {
			if (!e.getCode().equals("READY_HIT"))
				throw e;
		}
		assert true;
	}
}