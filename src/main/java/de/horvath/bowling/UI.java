package de.horvath.bowling;

import java.util.ArrayList;
import java.lang.reflect.Type;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import de.horvath.bowling.logic.Game;
import de.horvath.bowling.logic.LogicException;

/**
 * A simple command-line user interface to the logic.
 * 
 * @author Peter Horvath
 */
public class UI {
	/**
	 * The parsed, numerecial input data of the knocked pins.
	 */
	private ArrayList<ArrayList<Integer>> gameInput;
	
	/**
	 * The Game entity with the UI operates.
	 */
	private Game game;

	/**
	 * Parses the command line arguments and stores it in {@link #gameInput}.
	 * 
	 * @param args Command line arguments got from {@link de.horvath.bowling.App#main(String[])}
	 * 
	 * @throws LogicException {@link de.horvath.bowling.logic.LogicException}
	 */
	public void parseCmdlineInput(String[] args) throws LogicException {
		String knocks = StringUtils.join(args, " ");
		Gson gson = new Gson();
		Type tokenType = new TypeToken<ArrayList<ArrayList<Integer>>>(){}.getType();
		this.gameInput = gson.fromJson(knocks, tokenType);
		
		if (this.gameInput==null)
			throw new LogicException("INVALID_INPUT",
					"Invalid input format. You need to give a JSON-parsable two-dimensional integer-array.");
	}

	/**
	 * Constructs a {@link de.horvath.bowling.logic.Game} by inserting {@link #gameInput} into it.
	 * 
	 * @param game The Game entity to operate with.
	 * @throws LogicException @{link de.horvath.bowling.logic.LogicException}
	 */
	public void playGame(Game game) throws LogicException {
		this.game = game;
		for (ArrayList<Integer> frame : this.gameInput) {
			game.newFrame();
			for (Integer pinsKnockedDown : frame) {
				game.newBall(pinsKnockedDown);
			}
		}
	}

	/**
	 * Prints the calculated results to the stdout of the jvm.
	 * 
	 * @throws LogicException @{link de.horvath.bowling.logic.LogicException}
	 */
	public void printResultsToStdout() throws LogicException {
		Gson gson = new Gson();
		ArrayList<Integer> result = new ArrayList<Integer>();
		try {
			result = game.getResult();
		} catch (LogicException e) {
			e.throwRuntime();
		}
		String readableResult = gson.toJson(result);
		System.out.println(readableResult);
	}
}