package de.horvath.bowling.logic;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

/**
 * Handles an induvidual frame and its calculations.
 * 
 * @author Peter Horvath
 */
public class Frame {
	/**
	 * True, if this Frame is the tenth of the Game.
	 */
	private boolean tenth = false;
	
	/**
	 * The previous Frame of the Game. null, if it is the first.
	 */
	private Frame prevFrame = null;
	
	/**
	 * The next Frame of the Game. null, if it is the last.
	 */
	private Frame nextFrame = null;

	/**
	 * List of the number of the knocked down pin of the Balls of the Frame.
	 */
	private ArrayList<Integer> balls=new ArrayList<Integer>();

	/**
	 * Empty constructor.
	 */
	Frame() {
	}

	/**
	 * Getter to @{link #tenth}
	 * 
	 * @return boolean @{link #tenth}
	 */
	public boolean getTenth() {
		return tenth;
	}
	
	/**
	 * Setter of {@link #tenth}. Can only be set only once, and only from false to true.
	 * 
	 * @param tenth boolean @{link #tenth}
	 */
	public void setTenth(boolean tenth) {
		this.tenth = tenth;
	}
	
	/**
	 * Number of Balls currently existing in the Frame.
	 * 
	 * @return Integer The size of @{link #balls}
	 */
	public Integer getBallsNum() {
		return balls.size();
	}

	/**
	 * Getter of the previous Frame.
	 * 
	 * @return Frame {@link #prevFrame}
	 */
	public Frame getPrevFrame() {
		return this.prevFrame;
	}
	
	/**
	 * Setter of the previous Frame.
	 * 
	 * @param prevFrame {@link #prevFrame} 
	 */
	public void setPrevFrame(Frame prevFrame) {
		this.prevFrame = prevFrame;
	}
	
	/**
	 * Getter of the next Frame.
	 * .
	 * @return Frame {@link #nextFrame}
	 */
	public Frame getNextFrame() {
		return this.nextFrame;
	}
	
	/**
	 * Setter of the next Frame.
	 * 
	 * @param nextFrame {@link #nextFrame}
	 */
	public void setNextFrame(Frame nextFrame) {
		this.nextFrame = nextFrame;
	}
	
	/**
	 * Calculates the result of the current frame.
	 * 
	 * @return Integer The result. It can depend on the result of the next Frame(s), too.
	 * @throws LogicException {@link de.horvath.bowling.logic.LogicException}
	 */
	public Integer getResult() throws LogicException {
		if (!getReady())
			throw new LogicException("FRAME_NOT_READY_NO_RESULT",
					"Can't calculate result for a frame isn't ready yet");

		Integer result;
		
		if (!getTenth()) {
			if (getSpare()) {
				result = 10 + this.getNextFrame().sumBallsFrom(0, 1);
			} else if (getStrike()) {
				result = 10 + this.getNextFrame().sumBallsFrom(0, 2);
			} else {
				result = balls.get(0) + balls.get(1);
			}
		} else {
			if (getSpare()) {
				result = 10 + balls.get(2);
			} else if (getStrike()) {
				result = 10 + balls.get(1) + balls.get(2);
			} else {
				result = balls.get(0) + balls.get(1);
			}
		}
		
		return result;
	}

	/**
	 * @return boolean True iff this frame is a spare. It will be false if the Frame hasn't enough data yet
	 * to decide this.
	 */
	public boolean getSpare() {
		return !getStrike() && getBallsNum()>=2 && balls.get(0) + balls.get(1)==10;
	}
	
	/**
	 * @return boolean True iff this frame is a strike. It will be false if the Frame hasn't enough data yet
	 * to decide this.
	 */
	public boolean getStrike() {
		return getBallsNum()>0 && balls.get(0)==10;
	}
	
	/**
	 * @return boolean True iff the Frame has all of the needed Balls.
	 */
	public boolean getReady() {
		if (!getTenth()) {
			if (getSpare())
				return true;
			else if (getStrike())
				return true;
			else
				return getBallsNum()>=2;
		} else {
			if (getSpare())
				return getBallsNum()>=3;
			else if (getStrike())
				return getBallsNum()>=3;
			else
				return getBallsNum()>=2;
		}
	}

	/**
	 * @return Integer Calculates the currently standing pins in the Frame. Important remark:
	 * it wasn't obvious if a tenth Frame is a spare of a strike.
	 */
	public Integer getPinsLeft() {
		if (!getTenth()) {
			if (getSpare() || getStrike())
				return 0;
			else
				return 10-sumBallsFrom(0, getBallsNum());
		} else {
			if (getSpare())
				return 10-sumBallsFrom(2, getBallsNum()-2);
			else if (getStrike())
				return 10;
			else
				return 10-sumBallsFrom(0, getBallsNum());
		}
	}

	/**
	 * Adds a new Ball to the Frame. Will throw exception if it would result an illegal state.
	 * 
	 * @param pinsKnocked The number of the knocked pins.
	 * @throws LogicException @{link de.horvath.bowling.logic.LogicException}
	 */
	public void newBall(Integer pinsKnocked) throws LogicException {
		if (getReady())
			throw new LogicException("READY_HIT",
					"Ready frame "+toString()+" got an extra ball");

		if (getPinsLeft() < pinsKnocked)
			throw new LogicException("TOO_MANY_KNOCKS",
					"Frame "+toString()+" got a ball with more knocks ("+pinsKnocked+") as pin left ("+
							getPinsLeft()+")");
		
		this.balls.add(pinsKnocked);
	}

	/**
	 * Calculates the next ballNum balls, starting from the fromBall-th Ball of the Frame.
	 * 
	 * @param fromBall The counting will be from this Ball started.
	 * @param ballNum The number of Balls to count.
	 * @return The sum of scores of the counted Balls.
	 */
	public Integer sumBallsFrom(Integer fromBall, Integer ballNum) {
		Integer sum=0;
		
		Frame currentFrame = this;
		Integer currentBall = fromBall;
		for (Integer step=0; step<ballNum; step++) {
			sum+=currentFrame.balls.get(currentBall);
			if (++currentBall>=currentFrame.getBallsNum()) {
				currentBall=0;
				currentFrame=currentFrame.getNextFrame();
			}
		}
		
		return sum;
	}

	/**
	 * Common String-converter, mainly for debugging purposes.
	 */
	public String toString() {
		return "["+StringUtils.join(this.balls,",")+"]";
	}
}