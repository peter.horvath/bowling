package de.horvath.bowling.logic;

import java.util.ArrayList;

/**
 * Main class of the logic
 * 
 * @author Peter Horvath
 */
public class Game {
	/**
	 * Will be true after all of the needed Balls or all Frames is got.
	 */
	private boolean isInputProcessed = false;
	
	/**
	 * List of the Frames of the current Game scenario.
	 */
	private ArrayList<Frame> frames=new ArrayList<Frame>();

	/**
	 * Empty constructor.
	 */
	public Game() {
		
	}

	/**
	 * @return Number of the currently existing frames in the Game scenario.
	 */
	public Integer getFrameNum() {
		return frames.size();
	}

	/**
	 * @return Getter to the List of the Frames.
	 */
	public ArrayList<Frame> getFrames() {
		return this.frames;
	}
	
	/**
	 * @return The Frame last got by the Game entity.
	 * @throws LogicException @{link de.horvath.bowling.logic.LogicException}
	 */
	public Frame getLastFrame() throws LogicException {
		if (isEmpty())
			throw new LogicException("NO_FRAMES_YET",
				"Wants to get a frame while there isn't");
		
		return frames.get(frames.size()-1);
	}

	/**
	 * @return True iff the Game hasn't any frames yet.
	 */
	public boolean isEmpty() {
		return getFrameNum()==0;
	}

	/**
	 * @return True iff the Game has got the complete scenario.
	 */
	public boolean isInputProcessed() {
		return this.isInputProcessed;
	}
	
	/**
	 * Sets the game into an already processed state, no more
	 * results can be added, but result calculation can be done from now.
	 * 
	 * @throws LogicException @{link de.horvath.bowling.logic.LogicException}
	 */
	public void ready() throws LogicException {
		if (isInputProcessed())
			throw new LogicException("MULTIPLE_READY",
					"Game with multiple ending");
		this.isInputProcessed = true;
	}

	/**
	 * Creates a new Frame in the Game.
	 * 
	 * @return The newly added Frame.
	 * @throws LogicException @{link de.horvath.bowling.logic.LogicException}
	 */
	public Frame newFrame() throws LogicException {
		if (getFrameNum()>=10)
			throw new LogicException("TOO_MANY_FRAMES",
					"Logic got a new frame after it has already 10 of them");
		
		Frame lastFrame = null;
		if (!isEmpty()) {
			lastFrame = getLastFrame();
			if (!lastFrame.getReady())
				throw new LogicException("PREV_FRAME_NOT_READY",
					"Game got a new frame while the previous wasn't ready");
		}
		
		Frame frame = new Frame();

		if (!isEmpty()) {
			frame.setPrevFrame(lastFrame);
			lastFrame.setNextFrame(frame);
		}
		
		frames.add(frame);
		
		if (getFrameNum()==10)
			frame.setTenth(true);
		
		return frame;
	}
	
	/**
	 * Adds a new Ball to the last Frame.
	 * 
	 * @param pinsKnocked How many pins was knocked down by this Ball.
	 * @throws LogicException @{link de.horvath.bowling.logic.LogicException}
	 */
	public void newBall(Integer pinsKnocked) throws LogicException {
		if (pinsKnocked<0 || pinsKnocked>10)
			throw new LogicException("INVALID_KNOCK_NO",
					"Logic got a new Ball ("+pinsKnocked+") with knocks out of the 0..10 interval");
		
		getLastFrame().newBall(pinsKnocked);
		
		if (getFrameNum()==10 && getLastFrame().getReady())
			ready();
	}

	/**
	 * Calculates the results. Call only be called on a {@link ready} Game.
	 * 
	 * @return The results of the Frames in an ArrayList.
	 * @throws LogicException @{link de.horvath.bowling.logic.LogicException}
	 */
	public ArrayList<Integer> getResult() throws LogicException {
		if (!isInputProcessed())
			throw new LogicException("NO_RESULT",
					"Results aren't calculated yet");
		
		ArrayList<Integer> results = new ArrayList<Integer>();
		
		Integer lastResult = 0;
		
		for (Frame frame : getFrames()) {
			lastResult += frame.getResult();
			results.add(lastResult);
		}
		
		return results;
	}

	/**
	 * Textual, human-readable form of the Game scenario, mainly for debugging purposes.
	 * 
	 * @return String Easy understable data in JSON format.
	 */
	public String toString() {
		String result="[";
		for (Frame frame : getFrames())
			result+=frame.toString();
		result+="]";
		return result;
	}
}