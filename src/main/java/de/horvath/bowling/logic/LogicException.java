package de.horvath.bowling.logic;

/**
 * Exception class used by the core logic and the test cases.
 * 
 * @author Peter Horvath
 */
public class LogicException extends Exception {
	/**
	 * Simple error code to identify/test the exact problem.
	 */
	private String code;
	
	/**
	 * Textual, human-readable message of the error. Can contain debug data.
	 */
	private String message;

	/**
	 * Constructor of the Exception. The code and the message can only be set here.
	 * 
	 * @param code String @{link #code}
	 * @param message String @{link #message}
	 */
	public LogicException(String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	/**
	 * Getter of the error code.
	 * @return The error code in a String.
	 */
	public String getCode() {
		return this.code;
	}

	/**
	 * Getter of the human-readable message.
	 * @return The message in the String.
	 */
	public String getMessage() {
		return this.message;
	}

	/**
	 * Runtime, not catchable form of the Exception.
	 *  
	 * @return The RuntimeException entity.
	 */
	public RuntimeException getRuntime() {
		return new RuntimeException("Logic error: "+getMessage()+" ("+getCode()+")");
	}
	
	/**
	 * Re-throws the Exception in a not catch()-abe form.
	 */
	public void throwRuntime() {
		throw getRuntime();
	}
}