package de.horvath.bowling;

import de.horvath.bowling.logic.Game;
import de.horvath.bowling.logic.LogicException;

/**
 * Ten-pin bowling result calculator tool
 * 
 * @author Peter Horvath
 */
public class App {
	/**
	 * The main class of the App. Starts an UI and a Game, parses the command line arguments,
	 * calculates the result and prints it to the stdout of the jvm.
	 * 
	 * @param args The command line arguments.
	 * @throws LogicException @{link de.horvath.bowling.logic.LogicException}
	 */
    public static void main( String[] args ) throws LogicException {
    	UI ui = new UI();
    	Game game = new Game();
    	ui.parseCmdlineInput(args);
    	ui.playGame(game);
    	ui.printResultsToStdout();
    }
}