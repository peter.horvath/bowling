It is a simple command line application.

The dependent jars are compiled into target/bowling-1.0-full.jar .

The input must be given in command line argument(s) in the JSON format.

The description of the data structure is in the Assignment-Bowling-Game_new.pdf .

And example invocation:

java -jar target/bowling-1.0-full.jar '[[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2],[1,2]]'

And the result ([3,6,9,12,15,18,21,24,27,30]) will be printed in the next line.

The software was compiled and tested only with oracle java 1.8, but probably
it will work with other jvms as well.

There is also an API doc in the javadoc format in the target/site/apidocs directory.

You can recompile the tool with the common maven commands, I used

mvn clean package site

in the development.